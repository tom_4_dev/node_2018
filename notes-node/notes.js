const fs = require('fs');
console.log("starting notes js");

var fetchNotes = () => {
    try {
        notesString = fs.readFileSync('notes-data.json');
        notes = JSON.parse(notesString); 
        return notes;
    } catch (e) {
        return [];
    }
}

var saveNotes = (notes) => {
    fs.writeFileSync("notes-data.json", JSON.stringify(notes));
}

var addNote = (title, body) => {
    var notes = fetchNotes();
    var note = {
        title,
        body
    }
    var duplicateNotes = notes.filter((note) => note.title === title)

    if(duplicateNotes.length === 0) {
        notes.push(note);
        saveNotes(notes)
    }
};

var add = (a, b) => {
    return a + b;
}

var getAll = () => {
    console.log('Get all notes...');
    return fetchNotes();
}

var removeNote = (title) => {
    console.log("Removing note ", title);
    var notes = fetchNotes();
    var filterNotes = notes.filter((note) => note.title != title)
    saveNotes(filterNotes);
}

var getNote = (title) => {
    console.log("Fetching note for title", title);
    var notes = fetchNotes();
    
    var filterNotes = notes.filter((note) => note.title == title);
    return filterNotes;
}

module.exports = {
    addNote,
    add,
    getAll,
    removeNote,
    getNote
}