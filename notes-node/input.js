console.log('Starting input');
const yargs = require('yargs');
const notes = require('./notes.js');
const argv = yargs.argv;
var command = argv._[0];

console.log("Command: ", command);
console.log('Process', process.argv);
console.log('Yargs', argv);

if(command === "add") {
    console.log("Adding new note");
    notes.addNote(argv.title, argv.body);
} else if(command === "read") {
    console.log("Reading note");
    var allNotes = notes.getNote(argv.title);
    allNotes.forEach((note) => {
        console.log("title: ", note.title);
        console.log("body: ", note.body);
    })
} else if(command == "remove") {
    notes.removeNote(argv.title);
} else if(command == 'list') {
    var allNotes = notes.getAll();
    console.log(`Printing ${allNotes.length} all notes`);

    allNotes.forEach((note) => {
        console.log("title", note.title);
        console.log("body", note.body);
    });
} else {
    console.log("Command not recognized");
}
